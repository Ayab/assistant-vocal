# Assistant Vocal

**IDE:**

PyCharm : https://www.jetbrains.com/pycharm/download/#section=mac

**Configuration / installation des packages:**

On doit créer un environnement virtuel afin de ne pas modifier le dossier système.

-> python3 -m venv env

Le projet va donc être dans un dossier env

Ensuite installer les packages dont on a besoin

-> pip install speechrecognition (ce package a besoin du package pyaudio)

-> pip install gTTS (convert text to speech)

-> brew install portaudio puis python3 -m pip install pyaudio

Si pyaudio marche pas, lancer cette commande

pip install --global-option='build_ext' --global-option='-I/usr/local/include' --global-option='-L/usr/local/lib' pyaudio

-> pip install weathercom (météo)

-> pip install pygame (son)

-> pip install wikipedia

-> pip install googletrans


