import random
import wikipedia
import env.actions as a
import env.TextToSpeech as tts
import env.SpeechToText as stt


def execute_command(text):
    if "ça va" in text:
        tts.audio_playback('oui ça va')
        response = stt.speech_to_text("et vous? vous allez bien?")
        if "ça va" in response or "bien" in response:
            tts.audio_playback("c'est super!")
        if "non" in response or "pas vraiment" in response:
            tts.audio_playback("Oooh. J'espère que ça va aller mieux.")
    if "quel temps fait-il" in text:
        city = stt.speech_to_text('Quelle ville?')
        humidity, temp, phrase = a.weather_report(city)
        tts.audio_playback(
            "Actuellement à " + city + " la température est de " + str(temp) + " degrés, " + " et l'humidité est de " + str(
                humidity) + " pour cent")
    if "dis-moi une blague" in text:
        rand = random.randint(0, 2)
        if rand == 0:
            tts.audio_playback("Comment s’appelle la femelle du hamster? Hamsterdam!")
        if rand == 1:
            tts.audio_playback("Que se passe t’il quand deux poissons s’énervent? Le thon monte.")
        if rand == 2:
            tts.audio_playback("Connais-tu la blagues du chauffeur d’autobus ? non? bah moi non plus, j’étais derrière.")
    if "mets une musique" in text or "mets de la musique" in text:
        tts.audio_playback("Ok, je vais mettre de la musique")
        a.music()
    if "qui est" in text or "c'est quoi" in text or "c'est qui" in text or "où se trouve" in text or "qu'est-ce que" in text:
        search_data = text.split(" ")[2:]
        print(search_data)
        search_data = " ".join(search_data)
        print(search_data)
        tts.audio_playback("Ok, je cherche...")
        try:
            a.wikipedia_search(search_data)
        except wikipedia.exceptions.WikipediaException as e:
            tts.audio_playback("Désolée, je ne trouve pas!")
    if "il est quelle heure" in text or "quelle heure est-il" in text:
        c_time = a.current_time()
        tts.audio_playback("Il est " + c_time)
    if "on est quel jour" in text:
        today = a.current_date()
        tts.audio_playback("On est le " + today)
    if "peux-tu faire la traduction de" in text:
        search_data = text.split("peux-tu faire la traduction de ")
        print(search_data)
        search_data = ' '.join(search_data[1:])
        print(search_data)
        language = stt.speech_to_text('En quelle langue?')
        if "anglais" in language:
            print(language)
            a.translate_to_english(search_data)
        elif "espagnol" in language:
            print(language)
            a.translate_to_spanish(search_data)
    if "calculatrice" in text or "calculer" in text:
        element = stt.speech_to_text(tts.audio_playback("Dites-moi ce que vous voulez calculer. Par exemple : 2 plus 3"))
        calcul = a.calculate(*(element.split()))
        print(calcul)
        tts.audio_playback(element + ' est égal à ' + str(calcul))
    if "youtube" in text:
        a.open_youtube()
    if "google" in text:
        a.open_google()
    if "changer mon nom" in text:
        a.change_name()
        a.username()
        tts.audio_playback("Ok, c'est noté")
    if "bye" in text:
        tts.audio_playback("A bientôt")
        exit("Fin")
    #else :
        #tts.audio_playback("j'ai pas compris, peux-tu répéter?")
