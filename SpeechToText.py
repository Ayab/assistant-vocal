import speech_recognition as sr
import env.TextToSpeech as tts


r = sr.Recognizer()


def speech_to_text(ask=False):
    with sr.Microphone() as source:
        if ask:
            tts.audio_playback(ask)
        audio = r.listen(source, phrase_time_limit=5)
        text = ''
        try:
            text = r.recognize_google(audio, language="fr-FR")
        except sr.UnknownValueError as e:
            print(e)
        except sr.RequestError:
            print('service down')

        return text.lower()
