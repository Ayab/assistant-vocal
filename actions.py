import locale
import webbrowser
from datetime import datetime, date
import weathercom
import operator
import os
import json
import wikipedia
from googletrans import Translator
from pygame import mixer
import time
import env.TextToSpeech as tts
import env.SpeechToText as stt



#determine the operator
def operator_function(op):
    return {
        '+': operator.add,
        '-': operator.sub,
        'x': operator.mul,
        '/': operator.__truediv__
        }[op]


#calculate
def calculate(op1, op, op2):
    op1, op2 = int(op1), int(op2)
    return operator_function(op)(op1, op2)


#get the weather's report of a given city
def weather_report(city):
    weather_details = weathercom.getCityWeatherDetails(city)
    print(weather_details)
    humidity = json.loads(weather_details)['vt1observation']['humidity']
    temp = json.loads(weather_details)['vt1observation']['temperature']
    phrase = json.loads(weather_details)['vt1observation']['phrase']
    return humidity, temp, phrase


#search on wikipedia
def wikipedia_search(text):
    wikipedia.set_lang("fr")
    #search = wikipedia.search(text)
    #print(search)
    final = wikipedia.summary(text, sentences=2)
    print(final)
    tts.audio_playback("D'après Wikipedia " + final)


#english translation
def translate_to_english(text):
    translator = Translator()
    translation = translator.translate(str(text))  # english default
    tts.audio_playback(str(text) + '  en anglais est ')
    tts.audio_playback_english(translation.text)


#spanish translation
def translate_to_spanish(text):
    trans = Translator()
    translation = trans.translate(str(text), dest='es')
    tts.audio_playback(str(text) + '  en espagnol est ')
    tts.audio_playback_spanish(translation.text)


#open youtube
def open_youtube():
    path_chrome = webbrowser.get('chrome')
    tts.audio_playback("Ok. J'ouvre youtube")
    path_chrome.open('https://www.youtube.com/')


#open youtube
def open_google():
    path_chrome = webbrowser.get('chrome')
    tts.audio_playback("Ok. J'ouvre google")
    path_chrome.open('https://www.google.com/')


#function to search on wikipedia
def music():
    filename = 'bensound-cute.mp3'
    mixer.init()
    sound = mixer.Sound(filename)
    sound.play()
    time.sleep(11)
    sound.stop()


#get current time
def current_time():
    return datetime.now().strftime("%H:%M")


#get current date
def current_date():
    locale.setlocale(locale.LC_TIME, "fr_FR")
    today = date.today().strftime("%d %B %Y")
    return today


#set username
def username():
    first_name = stt.speech_to_text("Comment devrais-je vous appeler?")
    name = ''
    for character in first_name:
        if character.isalnum():
            name += character
    print(name)
    filename = 'name.txt'
    f = open(filename, "a")
    f.write(name)
    f.close()
    print("Bienvenue " + name)


def change_name():
    filename = 'name.txt'
    f = open(filename, "w")
    f.write("")
    f.close()


def first():
    filename = 'name.txt'
    if os.stat(filename).st_size == 0:
        tts.audio_playback("Bonjour")
        username()
    f = open(filename, "r")
    name = f.read()
    tts.audio_playback("Bienvenue " + name)
    tts.audio_playback("Je suis votre assistant Silver. Que puis-je faire pour vous?")