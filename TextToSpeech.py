import os
from gtts import gTTS
import playsound


def audio_playback(text):
    filename = "test.mp3"
    tts = gTTS(text=text, lang='fr-FR')
    tts.save(filename)
    playsound.playsound(filename)
    os.remove(filename)


def audio_playback_english(text):
    filename = "test1.mp3"
    tts = gTTS(text=text, lang='en-US')
    tts.save(filename)
    playsound.playsound(filename)
    os.remove(filename)


def audio_playback_spanish(text):
    filename = "test2.mp3"
    tts = gTTS(text=text, lang='es-ES')
    tts.save(filename)
    playsound.playsound(filename)
    os.remove(filename)
